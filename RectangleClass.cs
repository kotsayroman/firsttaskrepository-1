using System;

namespace Rectangle 
{
	// This class represents rectangle with sides parallel to coordinate axis.
	// Rectangle is represented by its top, left, bottom and right coordinates.
	class Rectangle
	{
		private double _top;
		private double _left;
		private double _bottom;
		private double _right;
		
		#region Constructors
		
		// Constructor with four parameters.
		// Rectangle(TOP, LEFT, RIGHT, BOTTOM).
		public Rectangle(double left, double top,  double right, double bottom)
		{
			if (left < right && top > bottom)
			{
				_top = top;
				_left = left;
				_bottom = bottom;
				_right = right;
			}
			else
			{
				throw new ArgumentException("Invalid coordinates");
			}
		}
		
		#endregion
		
		#region Priperties
		
		// Gets top coordinate.
		public double Top
		{
			get
			{
				return _top;
			}
			private set
			{
				_top = value;
			}
		}
		
		// Gets left coordinate.
		public double Left
		{
			get
			{
				return _left;
			}
			private set
			{
				_left = value;
			}
		}
		
		// Gets bottom coordinate.
		public double Bottom
		{
			get
			{
				return _bottom;
			}
			private set
			{
				_bottom = value;
			}
		}
		
		//  Gets right coordinate.
		public double Right
		{
			get
			{
				return _right;
			}
			private set
			{
				_right = value;
			}
		}
		
		//  Gets rectangle height.
		public double Height
		{
			get
			{
				return _top - _bottom;
			}
		}
		
		//  Gets rectangle width.
		public double Width
		{
			get
			{
				return _right - _left;
			}
		}
		#endregion
		
		#region Methods
		
		// Returns string representation of vector.
		// Rectangle is represented by its top-left and bottom-right points.
		public override string ToString()
		{
			return String.Format("[({1} {0}) ({3} {2})]", _top, _left, _bottom, _right);
		}
		
		// Moves the rectangle in horizontal direction and in vertical direction.
		public void Move(double horizontalDirection, double verticalDirection)
		{
			_top += verticalDirection;
			_left += horizontalDirection;
			_bottom += verticalDirection;
			_right += horizontalDirection;
		}
		
		// Changes the rectangle height and width in horizontal and vertical direction, remaining the same centre of rectangle.
		// Value of horizontal and vertical negative direction resizing can't be bigger than width and height of rectangle appropriately. 
		public void Resize(double horizontalResize, double verticalResize)
		{
			if ( (-horizontalResize) >= this.Width)
			{
				throw new ArgumentException("Invalid resize argument", "horizontalResize");
			}
			if ( (-verticalResize) >= this.Height)
			{
				throw new ArgumentException("Invalid resize argument", "verticalResize");
			}
			_top += verticalResize / 2.0;
			_left -= horizontalResize / 2.0;
			_bottom -= verticalResize / 2.0;
			_right += horizontalResize / 2.0;	
		}
		
		// Returns new smallest rectangle that contains two specified rectangles.
		public static Rectangle SmallestRectangleContainingTwo(Rectangle rect1, Rectangle rect2)
		{
			
			double top =  Math.Max(rect1.Top, rect2.Top);
			double bottom = Math.Min(rect1.Bottom, rect2.Bottom);
			
			double left = Math.Min(rect1.Left, rect2.Left);
			double right = Math.Max(rect1.Right, rect2.Right);
			
			return new Rectangle(left, top, right, bottom);
		}
		
		// Returns new rectangle that is obtained by intersection of two specified rectangles.
		// If rectangles don't intersect, null will be returned.
		public static Rectangle RectangleIntersection(Rectangle rect1, Rectangle rect2)
		{
			Rectangle probableRectangle = null;
			Rectangle intersectRectangle = null;
			
			try
			{
				probableRectangle = probableRectangleIntersection(rect1, rect2);
			}
			catch (ArgumentException)
			{
				//Console.WriteLine(" Exception caught in rectangle intersection: {0}", e.Message);
			}
			
			if ( probableRectangle != null)
			{
				try
				{
					intersectRectangle = probableRectangleIntersection(rect1, probableRectangle);
				}
				catch (ArgumentException)
				{
					//Console.WriteLine(" Exception caught in rectangle intersection: {0}", e.Message);
				}
			}
			return intersectRectangle;
		}
		#endregion
		
		#region PrivateMethods
		// Returns probable rectangle of intersection of two other.
		private static Rectangle probableRectangleIntersection(Rectangle rect1, Rectangle rect2)
		{
			
			double top =  Math.Min(rect1.Top, rect2.Top);
			double bottom = Math.Max(rect1.Bottom, rect2.Bottom);
			
			double left = Math.Max(rect1.Left, rect2.Left);
			double right = Math.Min(rect1.Right, rect2.Right);
			
			return new Rectangle(left, top, right, bottom);
		}
		#endregion
	}
	
	class Program
	{
		static void Main()
		{
			Console.WriteLine("\n\t Fun with RECTANGLES \n");
			
			Rectangle rectangle1 = new Rectangle(0, 10, 10, 0);
			Rectangle rectangle2 = new Rectangle(1, 2, 2, 1);
			Rectangle rectangle3 = new Rectangle(3, 6, 6, 3);
			Rectangle rectangle4 = new Rectangle(3, 6, 6, 3);
			Rectangle rectangle5 = new Rectangle(5, 5, 8, 4);
			Rectangle rectangle6 = new Rectangle(1, 8, 4, 5);
			
			Console.WriteLine("rectangle1 : {0}\n \tHeight = {1},\n \tWidth = {2}\n ", rectangle1, rectangle1.Height, rectangle1.Width);
			
			Rectangle rectangle7 = null;
			try
			{
				rectangle7 = new Rectangle(1, 2, 0, 1);
			}
			catch(ArgumentException e)
			{
				Console.WriteLine("Try to create rectangle with coordinates: 1, 2, 0, 1");
				Console.WriteLine(" Exception caught in rectangle constructor: {0}", e.Message);
			}
			
			// Moving:
			Console.WriteLine("\n\t Rectangle moving \n");
			Console.WriteLine(rectangle1);
			rectangle1.Move(5, 5);
			Console.WriteLine("Move: +5 +5 : {0}", rectangle1);
			
			rectangle1.Move(-5, 5);
			Console.WriteLine("Move: -5 +5 : {0}", rectangle1);
			
			rectangle1.Move(0, -10);
			Console.WriteLine("Move: 0 -10 : {0}", rectangle1);
			
			// Resizing:
			Console.WriteLine("\n\t Rectangle resizing \n");
			rectangle1.Resize(-4, 10);
			Console.WriteLine("Resize: -4 +10 : {0}", rectangle1);
			
			rectangle1.Resize(4, -10);
			Console.WriteLine("Resize back: 4 -10 : {0}", rectangle1);
			
			try
			{
				rectangle1.Resize(-10, 0);
			}
			catch(ArgumentException e)
			{
				Console.WriteLine("\nTry to decrease rectangle by 10 horizontally.");
				Console.WriteLine(" Exception caught in rectangle resizing: {0}", e.Message);
			}
			
			// Smallest Rectangle Containing Two Specified:
			Console.WriteLine("\n \t Smallest Rectangle Containing Two Specified \n");
			Rectangle unionRect1 = Rectangle.SmallestRectangleContainingTwo(rectangle1, rectangle2);
			Console.WriteLine(" {0} and {1} are in {2}", rectangle1, rectangle2, unionRect1);
			
			Rectangle unionRect2 = Rectangle.SmallestRectangleContainingTwo(rectangle3, rectangle4);
			Console.WriteLine(" {0} and {1} are in {2}", rectangle3, rectangle4, unionRect2);
			
			Rectangle unionRect3 = Rectangle.SmallestRectangleContainingTwo(rectangle4, rectangle5);
			Console.WriteLine(" {0} and {1} are in {2}", rectangle4, rectangle5, unionRect3);
			
			Rectangle unionRect4 = Rectangle.SmallestRectangleContainingTwo(rectangle4, rectangle6);
			Console.WriteLine(" {0} and {1} are in {2}", rectangle4, rectangle6, unionRect4);
			
			Rectangle unionRect5 = Rectangle.SmallestRectangleContainingTwo(rectangle5, rectangle6);
			Console.WriteLine(" {0} and {1} are in {2}", rectangle5, rectangle6, unionRect5);
			
			// Intersection of Two Specified Rectangles:
			Console.WriteLine("\n \t Intersection of Two Specified Rectangles \n");
			Rectangle intersectRect1 = Rectangle.RectangleIntersection(rectangle1, rectangle2);
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle1, rectangle2, intersectRect1);
			
			Rectangle intersectRect2 = Rectangle.RectangleIntersection(rectangle3, rectangle4);
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle3, rectangle4, intersectRect2);
			
			Rectangle intersectRect3 = Rectangle.RectangleIntersection(rectangle4, rectangle5);
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle4, rectangle5, intersectRect3);
			
			Rectangle intersectRect4 = Rectangle.RectangleIntersection(rectangle4, rectangle6);
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle4, rectangle6, intersectRect4);
			
			Rectangle intersectRect5 = Rectangle.RectangleIntersection(rectangle5, rectangle6);
			string intersecrString = intersectRect5 == null ? "null" : intersectRect5.ToString();
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle5, rectangle6, intersecrString);
			
			Rectangle intersectRect6 = Rectangle.RectangleIntersection(rectangle2, rectangle3);
			string intersecrString2 = intersectRect6 == null ? "null" : intersectRect6.ToString();
			Console.WriteLine(" {0} and {1} intersection is {2}", rectangle2, rectangle3, intersecrString2);
		}
	}
}